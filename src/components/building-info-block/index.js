import BuildingInfoBlock from './block'
import BuildingInfoBlockGroup from './group'

BuildingInfoBlock.Group = BuildingInfoBlockGroup

export {
  BuildingInfoBlockGroup
}

export default BuildingInfoBlock