```js static
// import { Link } from 'react-router-dom'
import { Breadcrumb } from 'creams-ui
```

```js
const routes = [{
  path: '/',
  breadcrumbName: '首页'
}, {
  path: '/',
  breadcrumbName: '一级面包屑'
}, {
  path: 'second',
  breadcrumbName: '当前页面'
}]

function itemRender(route, params, routes, paths) {
  const last = routes.indexOf(route) === routes.length - 1;
  return last ? <span>{route.breadcrumbName}</span> : <a href={paths.join('/')}>{route.breadcrumbName}</a>
}

<Breadcrumb itemRender={itemRender} routes={routes} />
```