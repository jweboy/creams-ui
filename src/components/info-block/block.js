import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'

require('./style/index.css')

/**
 * @author jianglei
 * @description 块组件
 * @example ./docs/block.md
 * @class InfoBlock
 * @extends {Component}
 */
class InfoBlock extends Component {
  static defaultProps = {
    prefixCls: 'creams-info-block',
    title: '标题',
    // titleChild: null,
    content: '-',
    children: null,
    onClick: (evt) => { },
    className: '',
    style: {},
  }
  static propTypes = {
    /** 
     * 前缀名
     * @ignore
     */
    prefixCls: PropTypes.string,
    /** 顶部小标题 */
    title: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.node
    ]),
    /** 顶部小标题说明 */
    // titleChild:PropTypes.oneOfType([
    //   PropTypes.string,
    //   PropTypes.node
    // ]),
    /** 底部内容 */
    content: PropTypes.any,
    /** 右侧扩展内容 */
    children: PropTypes.element,
    /** 点击函数 */
    onClick: PropTypes.func,
    /** 外部class覆盖 */
    className: PropTypes.string,
    /** 外部行内样式覆盖 */
    style: PropTypes.object,
  }
  static checkIsValidReactElement = (target) => React.isValidElement(target)
  renderContent = ({ prefixCls, content }) => { 
    if (InfoBlock.checkIsValidReactElement(content)) { 
      return content
    }
    if(content == null) { 
      return <spa>'-'</spa>
    }
  }
  render() {
    const { prefixCls, title, content, children, onClick, className, style } = this.props;
    const containerClasses = classnames({ [`${prefixCls}`]: prefixCls }, className)
    const isChildrenExist = !!children
    return (
      <div className={containerClasses} style={style} onClick={onClick}>
        <div className={`${prefixCls}__layout__${isChildrenExist ? 'inline' : 'block'}`}>
          <p className={`${prefixCls}-title`}>
            {
              InfoBlock.checkIsValidReactElement(title) ? 
                title :
                <span>{title}</span>
            }
            {
              // InfoBlock.checkIsValidReactElement(titleChild) ? 
              //   titleChild :
              //   <span>{titleChild}</span>
              }
          </p>  
          <p className={`${prefixCls}-content`}>
            {
              // undefined或null默认填充 '-'
              content == null ? '-' : content
            }
          </p>
        </div>  
        {children}
      </div>
    )
  }
}

export default InfoBlock