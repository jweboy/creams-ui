import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { Input, Button } from './components'

class App extends Component {
  render () {
    return (
      <div>
        <Input />
        <Button type="danger">click me</Button>
        <Button type="secondary">click me</Button>
      </div>
    )
  }
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
)