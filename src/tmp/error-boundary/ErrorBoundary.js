import React, { Component } from 'react'

class ErrorBoundary extends Component {
  constructor() {
    super()

    this.state = {
      hasError: false
    }
  }
  componentDidCatch(err, info) {
    console.log(err)
    console.log(info.componentStack);
    this.setState({
      hasError: true
    })
  }
  render() {
    if (this.state.hasError) {
      return <h1>Something weent wrong.</h1>
    }
    return this.props.children
  }
}

export default ErrorBoundary