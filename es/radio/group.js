import React from 'react'
import { Radio as AntRadio } from 'antd'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import './style/index.css'

const AntRadioGroup = AntRadio.Group


/**
 * @author jianglei
 * @example ./docs/group.md
 * @class RadioGroup
 * @extends {AntRadioGroup}
 */
class RadioGroup extends AntRadioGroup {
  static defaultProps = {
    defaultValue: '',
    name: '',
    options: '',
    size: 'default',
    value: '',
    onChange: (evt) => { },
    theme: 'light',
    customPrefixCls: 'creams-radio-group'
  }
  static propTypes = {
    /** 默认选中的值 */
    defaultValue: PropTypes.any,
    /** RadioGroup 下所有 input[type="radio"] 的 name 属性 */
    name: PropTypes.string,
    /** 
     * 以配置形式设置子元素 
     * @ignore
    */
    options: PropTypes.oneOfType([
      PropTypes.string,
      // TODO: 这里需要调整
      PropTypes.arrayOf(PropTypes.shape({
        label: PropTypes.string,
        value: PropTypes.string,
        disabled: PropTypes.bool
      })),
    ]),
    /** 
     * 大小，只对按钮样式生效 
     * @ignore
    */
    size: PropTypes.oneOf(['small', 'default', 'large']),
    /** 用于设置当前选中的值 */
    value: PropTypes.any,
    /** 选项变化时的回调函数 */
    onChange: PropTypes.func,
    /** 颜色主题 */
    theme: PropTypes.oneOf(['light', 'dark']),
    /** 
     * 组件前缀名
     * @ignore
    */
   customPrefixCls: PropTypes.string, 
  }
  render() {
    const { customPrefixCls, theme, children, ...otherProps } = this.props;
    const classes = classnames({
      [`${customPrefixCls}-${theme}`]: theme,
    })
    return (
      <AntRadioGroup className={classes} {...otherProps}>{children}</AntRadioGroup>
    )
  }
}

export default RadioGroup