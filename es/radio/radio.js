import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import { Radio as AntRadio } from 'antd'
import './style/index.css'

/**
 * @author jianglei
 * @description Radio组件包装
 * @example ./docs/radio.md
 * @class Radio
 * @extends {Component}
 */
class Radio extends AntRadio {
  static defaultProps = {
    autoFocus: false,
    checked: false,
    defaultChecked: false,
    value: '',
    disabled: false,
    customPrefixCls: 'creams-radio'
  }
  static propTypes = {
    /** 自动获取焦点 */
    autoFocus: PropTypes.bool,
    /** 指定当前是否选中 */
    checked: PropTypes.bool,
    /** 初始是否选中 */
    defaultChecked: PropTypes.bool,
    /** 根据 value 进行比较，判断是否选中 */
    value: PropTypes.any,
    /** 失效状态 */
    disabled: PropTypes.bool,
    /** 
     * 组件前缀名
     * @ignore
    */
   customPrefixCls: PropTypes.string, 
  }
  render() {
    const { customPrefixCls, ...otherProps } = this.props;
    const classes = classnames({
      [`${customPrefixCls}`]: customPrefixCls,
    })
    return (
      <AntRadio className={classes} {...otherProps}>
        {this.props.children}
      </AntRadio>
    )
  }
}

export default Radio