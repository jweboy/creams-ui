import React, { Component } from 'react'
import { matchUrl } from '../util'

const sizeTable = {
  'small': { width: 40, height: 40 },
  'default': { width: 54, height: 36 },
  'large': { width: 90, height: 56 },
}

// FIXME: image加载成功后增加透明度，这个点可续可以优化为默认深色系背景，图片加载成功后高斯模糊并清晰展示。

class IconImage extends Component {
  componentDidMount() {
    const { imageUrl, prefixCls } = this.props
    const loaderImage = new Image()

    loaderImage.src = imageUrl
    loaderImage.onload = () => {
      // TODO: 采用新的styleMap API改进
      this.imageNode.style.backgroundImage = `url('${imageUrl}')`
      this.imageNode.classList.add(`${prefixCls}-image-animate`)
    }
  }
  render () {
    const { prefixCls, size, imageUrl } = this.props
    if(!matchUrl(imageUrl)) {
      // console.warn('无效的image url, 请检查!');
      return null;
    }
    return (
      <div
        className={`${prefixCls}-${size}-image`}
        style={sizeTable[size]}
        ref={imageNode => { this.imageNode = imageNode; }}
      />
    )
  }
}

export default IconImage