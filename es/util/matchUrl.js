export default function matchUrl(str) {
  /* eslint-disable */
  const regexUrl = /(https?:\/\/)(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)/gi;
  return regexUrl.test(str)
}