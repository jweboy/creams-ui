module.exports = {
  presets: [
    "@babel/env",
    "@babel/react",
  ],
  plugins: [
    [require("@babel/plugin-proposal-class-properties"), { "loose": false }],
  ]
}