module.exports = {
  apps : [{
    name      : 'creams-ui',
    script    : 'npm',
    args      : 'run start',
    env: {
      NODE_ENV: 'development'
    },
    env_production : {
      NODE_ENV: 'production'
    },
    ignore_watch: [
      'node_modules'
    ],
  }],

  deploy : {
    production : {
      user: 'root',
      host: '118.24.155.105',
      ref  : 'origin/master',
      repo: 'git@gitlab.com:jweboy/creams-ui.git',
      path: '/home/www/creams-ui',
      ssh_options: 'StrictHostKeyChecking=no',
      'pre-deploy-local': "echo 'pm2部署测试'",
      'pre-deploy': 'git fetch && git pull origin master',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production',
    }
  }
};
