常用组合

```js
class Example extends React.Component{
  render() {
    return (
      <div style={{ display: 'flex' }}>
        <BuildingInfoBlock
          title="中赢国际"
          desc="杭州/滨江/长河街道"
          imageUrl="https://images.creams.io/2018/05/w0f2ctotcfl6ifzxk3scbi858f6ct9bn.jpg!/fw/200"
          theme="light"
          size="small"
          style={{ marginRight: 10 }}
        />
        <BuildingInfoBlock
          title="中大银泰城"
          desc="117.7万"
          imageUrl="https://images.creams.io/2018/05/9snwivxw7hp40kblcti1jdiqgohqzkxh.jpg!/fw/200"
          theme="dark"
          size="small"
          style={{ width: 180, marginRight: 10 }}
        />
        <BuildingInfoBlock
          title="aaa"
          desc="北京/北京"
          imageUrl="https://images.creams.io/2018/04/8aiitr2h4fzkpwwu1w1ars0ovibfr7r7.jpg!/fw/200"
          size="large"
          style={{ marginRight: 10 }}
        />
        <BuildingInfoBlock
          title="aaa"
          desc="北京/北京"
          type="normal"
        />
      </div>
    )
  }
}
;<Example />
```