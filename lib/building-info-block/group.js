"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames2 = _interopRequireDefault(require("classnames"));

var _icon = _interopRequireDefault(require("../icon"));

require("./style/index.css");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var initFunc = function initFunc() {};
/**
 * @author jianglei
 * @description 楼宇小部件包裹组件
 * @example ./docs/group.md
 * @class BuildingInfoBlockGroup
 * @extends {Component}
 */


var BuildingInfoBlockGroup =
/*#__PURE__*/
function (_Component) {
  _inherits(BuildingInfoBlockGroup, _Component);

  function BuildingInfoBlockGroup() {
    var _this;

    _classCallCheck(this, BuildingInfoBlockGroup);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(BuildingInfoBlockGroup).call(this));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "getMuiltipleData", function (map) {
      var ary = [];
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = map[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var _step$value = _slicedToArray(_step.value, 2),
              key = _step$value[0],
              value = _step$value[1];

          if (value) {
            ary.push(key);
          }
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return != null) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      return ary;
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "_setSingleHandler", function (index) {
      _this.setState(function (_ref) {
        var _toggleMap = _ref._toggleMap;
        var _iteratorNormalCompletion2 = true;
        var _didIteratorError2 = false;
        var _iteratorError2 = undefined;

        try {
          for (var _iterator2 = _toggleMap[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
            var _step2$value = _slicedToArray(_step2.value, 1),
                key = _step2$value[0];

            _toggleMap.set(key, false);
          }
        } catch (err) {
          _didIteratorError2 = true;
          _iteratorError2 = err;
        } finally {
          try {
            if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
              _iterator2.return();
            }
          } finally {
            if (_didIteratorError2) {
              throw _iteratorError2;
            }
          }
        }

        _toggleMap.set(index, true);

        _this.props.onClick(index);

        return {
          _toggleMap: _toggleMap
        };
      });
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "_setMultipleHandler", function (index) {
      _this.setState(function (_ref2) {
        var _toggleMap = _ref2._toggleMap;

        if (_toggleMap.has(index)) {
          var isActive = _toggleMap.get(index);

          _toggleMap.set(index, !isActive);
        } else {
          _toggleMap.set(index, true);
        }

        var keys = _this.getMuiltipleData(_toggleMap);

        _this.props.onClick(keys);

        return {
          _toggleMap: _toggleMap,
          _activeIndex: index
        };
      });
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "_toggleActive", function (index, single) {
      return function (evt) {
        // 事件代理
        var targetNodeName = evt.target.nodeName.toLocaleLowerCase();

        if (targetNodeName === 'img') {
          evt.stopPropagation();
        }

        if (single) {
          _this._setSingleHandler(index);
        } else {
          _this._setMultipleHandler(index);
        }
      };
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "renderChildren", function (props, state) {
      return _react.default.Children.map(props.children, function (reactElement, index) {
        // generate new element
        var _index = index + 1;

        var _indexKey = "$".concat(_index);

        var eventStyle = !!_index ? {
          cursor: 'pointer'
        } : {};
        var eventHandler = !!_index ? _this._toggleActive(_indexKey, props.single) : initFunc;

        var isCover = state._toggleMap.get(_indexKey);

        var imageSize = reactElement.props.size || 'default'; // current element

        var combineElement = _react.default.cloneElement(reactElement, {
          _index: _indexKey,
          _activeIndex: state._activeIndex,
          prefixCls: props.prefixCls
        }); // mask cover


        var iconElement = _react.default.createElement(_icon.default, {
          className: "".concat(props.prefixCls, "-icon"),
          type: "confirm",
          feedback: "normal"
        });

        var maskElement = _react.default.createElement('span', {
          className: isCover ? "".concat(props.prefixCls, "-mask-").concat(imageSize) : "".concat(props.prefixCls, "-mask-hidden")
        }, iconElement); // event container


        var eventElement = _react.default.createElement('div', {
          className: "".concat(props.prefixCls, "-event"),
          style: eventStyle,
          onClick: eventHandler
        }, [combineElement, maskElement]);

        return _react.default.cloneElement(eventElement);
      });
    });

    _this.state = {
      _toggleMap: new Map(),
      _activeIndex: null
    };
    return _this;
  }

  _createClass(BuildingInfoBlockGroup, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          prefixCls = _this$props.prefixCls,
          style = _this$props.style,
          className = _this$props.className;
      var classes = (0, _classnames2.default)(_defineProperty({}, "".concat(prefixCls, "-group"), "".concat(prefixCls, "-group")), className);
      return _react.default.createElement("div", {
        className: classes,
        style: style
      }, this.renderChildren(this.props, this.state));
    }
  }]);

  return BuildingInfoBlockGroup;
}(_react.Component);

_defineProperty(BuildingInfoBlockGroup, "defaultProps", {
  prefixCls: 'creams-buildingInfoBlock',
  style: {},
  className: '',
  onClick: function onClick(evt, record) {},
  single: true
});

_defineProperty(BuildingInfoBlockGroup, "propTypes", {
  /** 
   * 组件前缀名
   * @ignore
  */
  prefixCls: _propTypes.default.string,

  /** 外部行内样式覆盖 */
  style: _propTypes.default.object,

  /** 点击事件回调 */
  onClick: _propTypes.default.func,

  /** 外部class覆盖 */
  className: _propTypes.default.string,

  /** 单选或者多选 */
  single: _propTypes.default.bool
});

var _default = BuildingInfoBlockGroup;
exports.default = _default;