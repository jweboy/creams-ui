"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "BuildingInfoBlockGroup", {
  enumerable: true,
  get: function get() {
    return _group.default;
  }
});
exports.default = void 0;

var _block = _interopRequireDefault(require("./block"));

var _group = _interopRequireDefault(require("./group"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_block.default.Group = _group.default;
var _default = _block.default;
exports.default = _default;