"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames2 = _interopRequireDefault(require("classnames"));

var _iconImage = _interopRequireDefault(require("./icon-image"));

var _infoBlock = _interopRequireDefault(require("../info-block"));

require("./style/index.css");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * @author jianglei
 * @description 楼宇小部件
 * @example ./docs/block.md
 * @class BuildingInfoBlock
 * @extends {Component}
 */
var BuildingInfoBlock =
/*#__PURE__*/
function (_Component) {
  _inherits(BuildingInfoBlock, _Component);

  function BuildingInfoBlock() {
    _classCallCheck(this, BuildingInfoBlock);

    return _possibleConstructorReturn(this, _getPrototypeOf(BuildingInfoBlock).apply(this, arguments));
  }

  _createClass(BuildingInfoBlock, [{
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps, nextState) {
      // FIXME: 这步操作在多选情况下只render当前点击的元素,在单选情况下不render(只改变mask状态,无render也没影响)
      if (nextProps._activeIndex !== this.props._index) {
        return false;
      }

      return true;
    }
  }, {
    key: "render",
    value: function render() {
      var _classnames;

      var _this$props = this.props,
          title = _this$props.title,
          desc = _this$props.desc,
          prefixCls = _this$props.prefixCls,
          theme = _this$props.theme,
          style = _this$props.style,
          className = _this$props.className,
          size = _this$props.size,
          imageUrl = _this$props.imageUrl,
          type = _this$props.type,
          _index = _this$props._index;
      var classes = (0, _classnames2.default)((_classnames = {}, _defineProperty(_classnames, "".concat(prefixCls), prefixCls), _defineProperty(_classnames, "".concat(prefixCls, "-").concat(theme), theme), _defineProperty(_classnames, "".concat(prefixCls, "-interactive"), !!_index), _classnames), className);

      var styles = _objectSpread({
        border: type === 'default' ? '1px solid #d9d9d9' : 'none'
      }, style);

      return _react.default.createElement("div", {
        className: classes,
        style: styles
      }, _react.default.createElement(_iconImage.default, {
        size: size,
        imageUrl: imageUrl,
        prefixCls: prefixCls
      }), _react.default.createElement(_infoBlock.default, {
        title: _react.default.createElement("span", {
          className: "".concat(prefixCls, "-").concat(theme, "-title")
        }, title),
        content: _react.default.createElement("span", {
          className: "".concat(prefixCls, "-content")
        }, desc),
        className: "".concat(prefixCls, "-overwrite")
      }));
    }
  }]);

  return BuildingInfoBlock;
}(_react.Component);

_defineProperty(BuildingInfoBlock, "defaultProps", {
  title: '',
  desc: '',
  imageUrl: '',
  size: 'default',
  prefixCls: 'creams-buildingInfoBlock',
  theme: 'default',
  style: {},
  className: '',
  type: 'default'
});

_defineProperty(BuildingInfoBlock, "propTypes", {
  /** 标题 */
  title: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.node]),

  /** 描述 */
  desc: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.node]),

  /** 图片地址 */
  imageUrl: _propTypes.default.string,

  /** 图片大小 */
  size: _propTypes.default.oneOf(['default', 'small', 'large']),

  /** 
   * 组件前缀名
   * @ignore
  */
  prefixCls: _propTypes.default.string,

  /** 颜色主题 */
  theme: _propTypes.default.oneOf(['default', 'light', 'dark']),

  /** 外部行内样式覆盖 */
  style: _propTypes.default.object,

  /** 外部class覆盖 */
  className: _propTypes.default.string,
  type: _propTypes.default.oneOf(['default', 'normal'])
});

var _default = BuildingInfoBlock;
exports.default = _default;