"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "RadioGroup", {
  enumerable: true,
  get: function get() {
    return _group.default;
  }
});
exports.default = void 0;

var _radio = _interopRequireDefault(require("./radio"));

var _group = _interopRequireDefault(require("./group"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_radio.default.Group = _group.default;
var _default = _radio.default;
exports.default = _default;