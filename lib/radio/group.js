"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _antd = require("antd");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames2 = _interopRequireDefault(require("classnames"));

require("./style/index.css");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var AntRadioGroup = _antd.Radio.Group;
/**
 * @author jianglei
 * @example ./docs/group.md
 * @class RadioGroup
 * @extends {AntRadioGroup}
 */

var RadioGroup =
/*#__PURE__*/
function (_AntRadioGroup) {
  _inherits(RadioGroup, _AntRadioGroup);

  function RadioGroup() {
    _classCallCheck(this, RadioGroup);

    return _possibleConstructorReturn(this, _getPrototypeOf(RadioGroup).apply(this, arguments));
  }

  _createClass(RadioGroup, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          customPrefixCls = _this$props.customPrefixCls,
          theme = _this$props.theme,
          children = _this$props.children,
          otherProps = _objectWithoutProperties(_this$props, ["customPrefixCls", "theme", "children"]);

      var classes = (0, _classnames2.default)(_defineProperty({}, "".concat(customPrefixCls, "-").concat(theme), theme));
      return _react.default.createElement(AntRadioGroup, _extends({
        className: classes
      }, otherProps), children);
    }
  }]);

  return RadioGroup;
}(AntRadioGroup);

_defineProperty(RadioGroup, "defaultProps", {
  defaultValue: '',
  name: '',
  options: '',
  size: 'default',
  value: '',
  onChange: function onChange(evt) {},
  theme: 'light',
  customPrefixCls: 'creams-radio-group'
});

_defineProperty(RadioGroup, "propTypes", {
  /** 默认选中的值 */
  defaultValue: _propTypes.default.any,

  /** RadioGroup 下所有 input[type="radio"] 的 name 属性 */
  name: _propTypes.default.string,

  /** 
   * 以配置形式设置子元素 
   * @ignore
  */
  options: _propTypes.default.oneOfType([_propTypes.default.string, // TODO: 这里需要调整
  _propTypes.default.arrayOf(_propTypes.default.shape({
    label: _propTypes.default.string,
    value: _propTypes.default.string,
    disabled: _propTypes.default.bool
  }))]),

  /** 
   * 大小，只对按钮样式生效 
   * @ignore
  */
  size: _propTypes.default.oneOf(['small', 'default', 'large']),

  /** 用于设置当前选中的值 */
  value: _propTypes.default.any,

  /** 选项变化时的回调函数 */
  onChange: _propTypes.default.func,

  /** 颜色主题 */
  theme: _propTypes.default.oneOf(['light', 'dark']),

  /** 
   * 组件前缀名
   * @ignore
  */
  customPrefixCls: _propTypes.default.string
});

var _default = RadioGroup;
exports.default = _default;