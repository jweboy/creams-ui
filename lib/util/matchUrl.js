"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = matchUrl;

function matchUrl(str) {
  /* eslint-disable */
  var regexUrl = /(https?:\/\/)(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)/gi;
  return regexUrl.test(str);
}