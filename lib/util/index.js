"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "matchUrl", {
  enumerable: true,
  get: function get() {
    return _matchUrl.default;
  }
});

var _matchUrl = _interopRequireDefault(require("./matchUrl"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }